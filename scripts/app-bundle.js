define('app',['exports', './web-api'], function (exports, _webApi) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.App = undefined;

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	var App = exports.App = function () {
		function App() {
			_classCallCheck(this, App);
		}

		App.prototype.activate = function activate() {
			var api = new _webApi.WebAPI();
			api.fetchContacts();
		};

		App.prototype.configureRouter = function configureRouter(config, router) {
			config.title = 'Contacts';
			config.map([{ route: '', moduleId: 'no-selection', title: 'Welcome' }, { route: 'contacts/:id', moduleId: 'contact-detail', name: 'contacts' }]);

			this.router = router;
		};

		return App;
	}();
});
define('contact-detail',['exports', 'aurelia-event-aggregator', './web-api', './messages'], function (exports, _aureliaEventAggregator, _webApi, _messages) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ContactDetail = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _class, _temp;

  var ContactDetail = exports.ContactDetail = (_temp = _class = function () {
    function ContactDetail(api, ea) {
      _classCallCheck(this, ContactDetail);

      this.api = api;
      this.ea = ea;
    }

    ContactDetail.prototype.getContactList = function getContactList() {
      return new Promise(function (resolve) {
        var results = contacts.map(function (x) {
          return {
            id: x.id,
            firstName: x.firstName,
            lastName: x.lastName,
            photoUrlThumb: x.photoUrlThumb
          };
        });
        resolve(results);
      });
    };

    ContactDetail.prototype.activate = function activate(params, routeConfig) {
      console.log(params);
      this.routeConfig = routeConfig;
    };

    return ContactDetail;
  }(), _class.inject = [_webApi.WebAPI, _aureliaEventAggregator.EventAggregator], _temp);
});
define('contact-list',['exports', 'aurelia-fetch-client'], function (exports, _aureliaFetchClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ContactList = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _class, _temp;

  var id = 0;

  var ContactList = exports.ContactList = (_temp = _class = function () {
    function ContactList(http) {
      _classCallCheck(this, ContactList);

      this.contacts = [];
      this.http = http;
      this.fetchUserData();
    }

    ContactList.prototype.getId = function getId() {
      return ++id;
    };

    ContactList.prototype.fetchUserData = function fetchUserData() {
      var _this = this;

      this.http.fetch('https://randomuser.me/api/?results=100').then(function (results) {
        return results.json();
      }).then(function (data) {
        console.log(data.results);
        var i,
            results = data.results;
        for (i = 0; i < results.length; i++) {
          _this.contacts.push({
            id: _this.getId(),
            params: results[i].id.name,
            firstName: _this.toTitleCase(results[i].name.first),
            lastName: _this.toTitleCase(results[i].name.last),
            gender: results[i].gender,
            city: _this.toTitleCase(results[i].location.city),
            email: results[i].email,
            phoneNumber: results[i].phone,
            photoUrlThumb: results[i].picture.thumbnail,
            photoUrlFull: results[i].picture.large });
        }
      });
    };

    ContactList.prototype.select = function select(contact) {
      console.log(contact);
      this.selectedId = contact.id;
      return true;
    };

    ContactList.prototype.getContactList = function getContactList() {
      return new Promise(function (resolve) {
        var results = contacts.map(function (x) {
          return {
            id: x.id,
            firstName: x.firstName,
            lastName: x.lastName,
            photoUrlThumb: x.photoUrlThumb
          };
        });
        resolve(results);
      });
    };

    ContactList.prototype.toTitleCase = function toTitleCase(str) {
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };

    ContactList.prototype.getContactDetails = function getContactDetails(id) {
      return new Promise(function (resolve) {
        var found = contacts.filter(function (x) {
          return x.id == id;
        })[0];
        console.log(found);
        resolve(JSON.parse(JSON.stringify(found)));
      });
    };

    return ContactList;
  }(), _class.inject = [_aureliaFetchClient.HttpClient], _temp);
});
define('environment',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    debug: true,
    testing: true
  };
});
define('main',['exports', './environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;

  var _environment2 = _interopRequireDefault(_environment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  Promise.config({
    warnings: {
      wForgottenReturn: false
    }
  });

  function configure(aurelia) {
    aurelia.use.standardConfiguration().feature('resources');

    if (_environment2.default.debug) {
      aurelia.use.developmentLogging();
    }

    if (_environment2.default.testing) {
      aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(function () {
      return aurelia.setRoot();
    });
  }
});
define('messages',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var ContactViewed = exports.ContactViewed = function ContactViewed(contact) {
    _classCallCheck(this, ContactViewed);

    this.contact = contact;
    console.log(contact);
  };
});
define('no-selection',["exports"], function (exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var NoSelection = exports.NoSelection = function NoSelection() {
        _classCallCheck(this, NoSelection);
    };
});
define('web-api',['exports', 'aurelia-fetch-client'], function (exports, _aureliaFetchClient) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
				value: true
		});
		exports.WebAPI = undefined;

		function _classCallCheck(instance, Constructor) {
				if (!(instance instanceof Constructor)) {
						throw new TypeError("Cannot call a class as a function");
				}
		}

		var NUM_CONTACTS = 100;

		var latency = 200;
		var id = 0;

		function getId() {
				return ++id;
		}

		var contacts = [];

		var WebAPI = exports.WebAPI = function () {
				function WebAPI() {
						_classCallCheck(this, WebAPI);
				}

				WebAPI.prototype.fetchContacts = function fetchContacts() {
						var _this = this;

						var httpClient = new _aureliaFetchClient.HttpClient();
						httpClient.configure(function (config) {
								config.withBaseUrl("https://randomuser.me/api/").withDefaults({
										credentials: 'same-origin',
										headers: {
												'Accept': 'application/json',
												'X-Requested-With': 'Fetch'
										}
								}).withInterceptor({
										request: function request(_request) {
												console.log('Requesting ' + _request.method + ' ' + _request.url);
												return _request;
										},
										response: function response(_response) {
												console.log('Received ' + _response.status + ' ' + _response.url);
												return _response;
										}
								});
						});

						httpClient.fetch("?results=" + NUM_CONTACTS).then(function (response) {
								return response.json();
						}).then(function (data) {
								var i,
								    results = data.results;
								for (i = 0; i < results.length; i++) {
										contacts.push({
												id: getId(),
												firstName: _this.toTitleCase(results[i].name.first),
												lastName: _this.toTitleCase(results[i].name.last),
												gender: results[i].gender,
												city: _this.toTitleCase(results[i].location.city),
												email: results[i].email,
												phoneNumber: results[i].phone,
												photoUrlThumb: results[i].picture.thumbnail,
												photoUrlFull: results[i].picture.large });
								}
						});
				};

				WebAPI.prototype.toTitleCase = function toTitleCase(str) {
						return str.replace(/\w\S*/g, function (txt) {
								return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
						});
				};

				WebAPI.prototype.select = function select(contact) {
						this.selectedId = contact.id;
						return true;
				};

				WebAPI.prototype.getContactList = function getContactList() {

						return new Promise(function (resolve) {
								setTimeout(function () {
										var results = contacts.map(function (x) {
												return {
														id: x.id,
														firstName: x.firstName,
														lastName: x.lastName,
														photoUrlThumb: x.photoUrlThumb
												};
										});
										resolve(results);
								}, latency);
						});
				};

				return WebAPI;
		}();
});
define('resources/index',["exports"], function (exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.configure = configure;
    function configure(config) {}
});
define('contact-http',[], function () {
  "use strict";
});
define('text!app.html', ['module'], function(module) { module.exports = "<template><require from=\"./styles.css\"></require><require from=\"./contact-list\"></require><a href=\"#\">Home</a><div id=\"contact-browser\"><contact-list id=\"browser-contact-list\"></contact-list><router-view id=\"browser-router-view\"></router-view></div></template>"; });
define('text!styles.css', ['module'], function(module) { module.exports = ".detail, .field, .no-selection {\n  margin: 20px;\n}\n\nbody {\n\tmargin: 70px;\n}\n\n#browser-router-view {\n  flex: 1;\n}\n#contact-browser {\n  display: flex;\n}\n#browser-contact-list {\n  width: 500px;\n}\n\n.contact-list {\n  overflow-y: scroll;\n  height: 500px;\n  padding: 10px;\n}\n\nli.list-group-item {\n  list-style: none;\n  color: navy;\n  font-size: large;\n}\nli.list-group-item > a {\n  text-decoration: none;\n}\nli.list-group-item.active {\n  background: #A9D0F5;\n}\na:focus {\n  outline: none;\n}\n.thumbnail-div {\n  display: inline-block;\n}\n.thumbnail {\n\tmargin: 10px 10px 0px 10px; \n}\n\nh1 {\n\tmargin: 15px;\n}\n\n.profile-pic {\n\tdisplay: block;\n\tmargin: 15px;\n}\n.gender {\n\theight: 2%;\n\twidth: 2%;\n\tmargin-left: 60px;\n}\n.field-name {\n  width: 200px;\n  display: inline-block;\n}\n\n"; });
define('text!contact-detail.html', ['module'], function(module) { module.exports = "<template><div class=\"detail\"><h1>${contact.firstName}&nbsp${contact.lastName}</h1><img class=\"profile-pic\" src=\"${contact.photoUrlFull}\"> <img class=\"gender\" src=\"https://upload.wikimedia.org/wikipedia/commons/${contact.gender === 'male' ? 'f/fd/Symbol_mars.svg' : '6/61/Symbol_venus_blue.svg'}\"><div><div class=\"field\"><span class=\"field-name\">First Name:</span>${contact.firstName}</div><div class=\"field\"><span class=\"field-name\">Last Name:</span>${contact.lastName}</div><div class=\"field\"><span class=\"field-name\">City:</span>${contact.city}</div><div class=\"field\"><span class=\"field-name\">Email:</span>${contact.email}</div><div class=\"field\"><span class=\"field-name\">Phone Number:</span>${contact.phoneNumber}</div></div></div></template>"; });
define('text!contact-list.html', ['module'], function(module) { module.exports = "<template><div class=\"contact-list\"><ul><a repeat.for=\"contact of contacts\" route-href=\"route: contacts; params.bind: {id:contact.id}\" click.delegate=\"$parent.select(contact)\"><li class=\"list-group-item ${contact.id === $parent.selectedId ? 'active' : ''}\"><div class=\"thumbnail-div\"><img class=\"thumbnail\" src=\"${contact.photoUrlThumb}\">${contact.firstName} ${contact.lastName}</div></li></a></ul></div></template>"; });
define('text!no-selection.html', ['module'], function(module) { module.exports = "<template><div class=\"no-selection\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec neque non mi sodales egestas. Nulla aliquet tincidunt mi dignissim rutrum. Cras rutrum mollis dolor malesuada consectetur. Fusce sed ullamcorper lacus, ac congue mi. Proin nulla risus, consectetur ut ultricies id, rutrum ut nisl. Etiam porttitor malesuada nunc, in ullamcorper sem iaculis sed. Aliquam id erat a orci pellentesque auctor.</p><p>Mauris sit amet leo sed sapien aliquet egestas. Vestibulum quis placerat nibh. Aenean ut purus tincidunt, elementum est pharetra, lobortis ex. Donec tincidunt lacus eget accumsan sollicitudin. Ut vulputate suscipit leo a sodales. In hac habitasse platea dictumst. Curabitur ornare metus sed ipsum viverra, quis molestie purus gravida. Aenean sagittis molestie fringilla. Donec posuere vel metus sit amet feugiat. Quisque posuere nisl et lacus tincidunt, ac interdum nulla commodo. Quisque dictum elit nec diam pulvinar, sed accumsan justo suscipit. Aenean id lorem semper, commodo lectus et, egestas lorem. Ut dignissim sem non quam euismod bibendum. Quisque sed justo elit. Aliquam hendrerit finibus velit vitae pretium. Aliquam sed nisl at enim vehicula porta vel eget neque.</p><p>Nam sed bibendum nibh, in congue purus. Nam elementum neque ac convallis consectetur. Morbi eu eros in nunc blandit cursus. Suspendisse finibus porta pharetra. Aenean sit amet ligula libero. Nullam at facilisis diam. Praesent elementum ultrices elit, ut ornare tortor dictum nec. Cras efficitur molestie dignissim. Maecenas magna mauris, vestibulum vel ultricies at, fringilla tempus augue.</p><p>For these, and many other reasons, this directory is particularly useful !</p></div></template>"; });
//# sourceMappingURL=app-bundle.js.map