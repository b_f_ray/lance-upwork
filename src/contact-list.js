
import {HttpClient} from 'aurelia-fetch-client';

let id = 0;

export class ContactList {
  static inject = [HttpClient];

  constructor(http){
    this.contacts = [];
    this.http = http;
    this.fetchUserData();
  }

  getId(){
    return ++id;
  }

  fetchUserData(){
    this.http.fetch('https://randomuser.me/api/?results=100')
    .then(results => results.json())
    .then(data => {
      console.log(data.results);
      var i, results = data.results;
      for (i = 0; i < results.length; i++) {
        this.contacts.push({
          id:this.getId(),
          params: results[i].id.name,
          firstName:this.toTitleCase(results[i].name.first),
          lastName:this.toTitleCase(results[i].name.last),
          gender:results[i].gender,
          city:this.toTitleCase(results[i].location.city),
          email:results[i].email,
          phoneNumber:results[i].phone,
          photoUrlThumb:results[i].picture.thumbnail,
          photoUrlFull:results[i].picture.large });
      }
    })
  }

  select(contact){
    console.log(contact);
    this.selectedId = contact.id;
    return true;
  }

  getContactList(){
	    return new Promise(resolve => {
	        let results = contacts.map(x =>  { return {
	          id:x.id,
	          firstName:x.firstName,
	          lastName:x.lastName,
	          photoUrlThumb:x.photoUrlThumb
	        }});
	        resolve(results);
	    });
  }

  toTitleCase(str){
      return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }
  getContactDetails(id){
    return new Promise(resolve => {
        let found = contacts.filter(x => x.id == id)[0];
        console.log(found);
        resolve(JSON.parse(JSON.stringify(found)));
    });
  }

}
