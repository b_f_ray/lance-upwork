import {WebAPI} from './web-api';

export class App {

	activate() {
		var api = new WebAPI();
		api.fetchContacts();
	}
	
	configureRouter(config, router){
	    config.title = 'Contacts';
	    config.map([
	      { route: '',              moduleId: 'no-selection',   title: 'Welcome'},
	      { route: 'contacts/:id',  moduleId: 'contact-detail', name:'contacts' }
	    ]);
	
	    this.router = router;
	}
}

  