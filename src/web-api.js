import {HttpClient, json} from 'aurelia-fetch-client';

let NUM_CONTACTS = 100;

let latency = 200;
let id = 0;

function getId(){
  return ++id;
}

let contacts = [];

export class WebAPI {

  fetchContacts() {
	  var httpClient = new HttpClient();
	  httpClient.configure(config => {
		  config
	        .withBaseUrl("https://randomuser.me/api/")
	        .withDefaults({
	        	credentials: 'same-origin',
	        	headers: {
	        		'Accept': 'application/json',
	        		'X-Requested-With': 'Fetch'
	            }
	         })
	         .withInterceptor({
	            request(request) {
	              console.log(`Requesting ${request.method} ${request.url}`);
	              return request;
	         },
	         response(response) {
	              console.log(`Received ${response.status} ${response.url}`);
	              return response;
	         }
	     });
	  });

	  httpClient.fetch("?results="+NUM_CONTACTS)
	      .then(response => response.json())
	      .then(data => {
	    	  var i, results = data.results;
	    	  for (i = 0; i < results.length; i++) {
	    		  contacts.push({
	    			  id:getId(),
	    			  firstName:this.toTitleCase(results[i].name.first),
	    			  lastName:this.toTitleCase(results[i].name.last),
	    			  gender:results[i].gender,
	    			  city:this.toTitleCase(results[i].location.city),
	    			  email:results[i].email,
	    			  phoneNumber:results[i].phone,
	    			  photoUrlThumb:results[i].picture.thumbnail,
	    			  photoUrlFull:results[i].picture.large });
	    		  	//  console.log(results[i]);
	    	  }
	  });
  }

  toTitleCase(str){
      return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }

  select(contact){
    this.selectedId = contact.id;
    return true;
  }

  getContactList(){

	    return new Promise(resolve => {
	      setTimeout(() => {
	        let results = contacts.map(x =>  { return {
	          id:x.id,
	          firstName:x.firstName,
	          lastName:x.lastName,
	          photoUrlThumb:x.photoUrlThumb
	        }});
	        resolve(results);
	      }, latency);
	    });
  }


}
