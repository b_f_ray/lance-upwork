import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import {ContactViewed} from './messages';

export class ContactDetail {
  static inject = [WebAPI, EventAggregator];

  constructor(api, ea){
    this.api = api;
    this.ea = ea;
    this.contacts = this.api.contacts;
    console.log(this.contacts);
  }

  getContactList(){
	    return new Promise(resolve => {
	        let results = contacts.map(x =>  { return {
	          id:x.id,
	          firstName:x.firstName,
	          lastName:x.lastName,
	          photoUrlThumb:x.photoUrlThumb
	        }});
	        resolve(results);
	    });
  }

  activate(params, routeConfig) {
    console.log(params)
    this.routeConfig = routeConfig;

  //  return this.api.getContactDetails(params.id).then(contact => {
  //    this.contact = contact;
  //    this.routeConfig.navModel.setTitle(contact.firstName + " " + contact.lastName);
  //    this.ea.publish(new ContactViewed(this.contact));
  //  });
  }
}
